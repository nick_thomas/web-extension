const path = require("path");
const SizePlugin = require("size-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");

module.exports = {
  devtool: "source-map",
  stats: "errors-only",
  entry: {
    content: "./src/content.ts",
    background: "./src/background.ts",
  },
  output: {
    path: path.join(__dirname, "dist"),
    filename: "[name].js",
  },
  plugins: [
    /* new SizePlugin(), */
    new CopyPlugin({
      patterns: [
        // {
        // from: "**/*",
        // context: "src",
        // globOptions: {
        // ignore: ["*.js"],
        //},
        // },
        {
          from:
            "node_modules/webextension-polyfill/dist/browser-polyfill.min.js",
        },
      ],
    }),
  ],
  optimization: {
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          mangle: false,
          compress: false,
          output: {
            beautify: true,
            indent_level: 2, // eslint-disable-line camelcase
          },
        },
      }),
    ],
  },
  module: {
    rules: [
      {
        test: /\.(js|ts|tsx)$/,
        loader: "ts-loader",
        exclude: /node_modules/,
      },
    ],
  },
};
