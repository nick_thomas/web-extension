
function handleMessage(request: any, sender: any, sendResponse: any) {
    browser.notifications.create({
        type: "basic",
        title: request.song,
        message: request.artist + request.meta,
        iconUrl: browser.extension.getURL("icon.png"),
    });
    sendResponse({ response: request.song });
}
browser.runtime.onMessage.addListener(handleMessage);

type CreateNotificationOptions = browser.notifications.CreateNotificationOptions;

function getId(id: string) {
    // [key:string] -> Defines the keys which are in the object, in the following they are in strings which are stored as number
    return function logNotifications(all:  {[key: string]: CreateNotificationOptions }): Promise<NotificationObject> {
        return new Promise((resolve, reject) => {
            let foundID: NotificationObject;
            for (let idx in all) {
                if (idx == id) {
                    foundID = all[idx] as NotificationObject;
                    resolve(foundID);
                }
            }
            reject(undefined)
        })


    }
}

type Port = browser.runtime.Port;
let portFromCS: Port;

function connected(p: Port) {
    portFromCS = p;
}

browser.runtime.onConnect.addListener(connected);

export type NotificationObject = {
    iconUrl: string,
    message: string,
    title: string,
    type: string
}
const clickedNotification = (notification: string) => {
    let anotherNotification = getId(notification);
    browser.notifications.getAll().then(anotherNotification).then((notification: NotificationObject) => {
        if (notification != undefined) {
            portFromCS.postMessage({ ...notification });

        }
    }).catch(e => console.log(e));
}
browser.notifications.onClicked.addListener(clickedNotification);