import { NotificationObject } from "./background";

function notifyBackgroundPage(e: Event) {
  if (e && e.target) {
    let items = (e.target as Element).querySelectorAll('span');
    let props = [...items].filter(element => element.classList.value == "" && element.attributes.length == 0);
    if (props.length < 2) {
      return
    }
    let { song, artist, meta } = { song: "", artist: "", meta: "" };
    let newProps = [...new Set(props)];
    for (let index = 0; index < newProps.length; index += 1) {
      switch (true) {
        case (index === 0):
          song += newProps[index].textContent;
          break;
        case (index === 1):
          artist += newProps[index].textContent;
          break;
        case (index === 2):
          artist += " " + newProps[index].textContent;
          break;
        default:
          meta += " " + newProps[index].textContent;
          break;
      }

    }
    const sending = browser.runtime.sendMessage({
      song, artist, meta
    });
  }
}

if (window.location.toString().includes("spotify")) {
  let myPort = browser.runtime.connect({ name: "port-from-cs" });

  myPort.onMessage.addListener(function (comm: unknown) {
    if (comm as NotificationObject) {
      let comme = (comm as NotificationObject);
      window.location.replace(`https://music.apple.com/in/search?term=${comme.title} ${comme.message}`);
    }
  });

  window.addEventListener("click", notifyBackgroundPage as any);
} 